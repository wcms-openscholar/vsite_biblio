# Vsite Biblio

Allows users to choose the biblio style for their site.

## Getting Started

Download and enable module.

### Prerequisites

Open Scholar base profile.

## Authors

* **Eric Bremner** - *Initial work* - [University of Waterloo](https://uwaterloo.ca)
