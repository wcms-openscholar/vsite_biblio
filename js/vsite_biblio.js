(function ($) {

    Drupal.behaviors.uw_cfg_openscholar = {

        attach: function (context, settings) {

            $('#biblio_style').change(function() {
                // Make the ajax call, set to synchronous so that we wait to get a response.
                var new_style_html;
                $.ajax({
                    type: 'get',
                    async: false,
                    url: Drupal.settings.basePath + "?q=vsite_biblio/citation_example&new_biblio_style=" + $(this).val(),
                    // If we have success set to the global variable, so that we can use it later to check for errors.
                    success: function(data) {
                        new_style_html = data;
                    }
                });
                if($('.form-item-biblio-style #new-biblio-style').length == 0) {
                    $('.form-item-biblio-style .description').append('<div id="new-biblio-style"><b>New Citation Style:</b><br />' + new_style_html + '<div>');
                }
                else {
                    $('.form-item-biblio-style #new-biblio-style').html('<b>New Citation Style:</b><br />' + new_style_html);
                }
            });
        }
    };
})(jQuery);